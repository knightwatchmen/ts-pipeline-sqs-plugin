import { SQS } from 'aws-sdk';
import { MortarPlugin, Contract } from "@pythagoras/ts-pipeline";
export declare class SQSMortarPlugin extends MortarPlugin {
    private consumer;
    private queue;
    private url;
    private region;
    constructor();
    initialize(sqs: SQS, url: string, region: string): void;
    private initializeConsumer();
    private handleSQSmessage(message, done);
    private sendSQSMessage(message);
    write<A extends Contract>(chunk: A): Promise<boolean>;
    resume(): void;
    pause(): void;
}
