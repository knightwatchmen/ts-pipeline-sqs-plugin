"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var sqsConsumer = require("sqs-consumer");
var ts_pipeline_1 = require("@pythagoras/ts-pipeline");
var SQSMortarPlugin = (function (_super) {
    __extends(SQSMortarPlugin, _super);
    function SQSMortarPlugin() {
        return _super.call(this) || this;
    }
    SQSMortarPlugin.prototype.initialize = function (sqs, url, region) {
        this.queue = sqs;
        this.url = url;
        this.region = region;
        this.initializeConsumer();
    };
    SQSMortarPlugin.prototype.initializeConsumer = function () {
        this.consumer = sqsConsumer.create({
            queueUrl: this.url,
            region: this.region,
            sqs: this.queue,
            messageAttributeNames: [],
            handleMessage: this.handleSQSmessage.bind(this),
        });
        this.consumer.on('processing_error', function (error, message) {
            console.log(error.message);
        });
        return true;
    };
    SQSMortarPlugin.prototype.handleSQSmessage = function (message, done) {
        var contract;
        try {
            contract = JSON.parse(message.Body);
        }
        catch (err) {
            done(new Error('Unable to parse body.'));
        }
        this.emit('data', {
            contract: contract,
            callback: done,
        });
        done();
    };
    SQSMortarPlugin.prototype.sendSQSMessage = function (message) {
        var _this = this;
        var params = {
            MessageBody: JSON.stringify(message),
            QueueUrl: this.url,
            MessageAttributes: {},
        };
        return new Promise(function (resolve, reject) {
            _this.queue.sendMessage(params, function (err, data) {
                if (err) {
                    reject(err);
                }
                resolve(true);
            });
        });
    };
    SQSMortarPlugin.prototype.write = function (chunk) {
        return this.sendSQSMessage(chunk)
            .then(function (result) {
            return true;
        })
            .catch(function (error) {
            //TODO: handle Errors
        });
    };
    SQSMortarPlugin.prototype.resume = function () {
        this.consumer.start();
    };
    SQSMortarPlugin.prototype.pause = function () {
        this.consumer.stop();
    };
    return SQSMortarPlugin;
}(ts_pipeline_1.MortarPlugin));
exports.SQSMortarPlugin = SQSMortarPlugin;
//# sourceMappingURL=index.js.map